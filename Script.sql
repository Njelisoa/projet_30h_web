create table utilisateur (
    id int primary key  auto_increment,
    mail varchar (30) not null ,
    motDePasse VARCHAR (255) not null ,
    statut int check (statut=0 or statut=1)
);

create table CategorieT ( 
    id int primary key  auto_increment,
    nom varchar(30) not null,
    rendement double default 15,
    CHECK (rendement>=0)
);

create table parcelle (
    id int primary key  auto_increment,
    taille double ,
    idCategorieT int REFERENCES CategorieT,
    Dates date not null
);

create table Cueilleur (
    id int primary key  auto_increment,
    nom varchar (40)
);


create table Cueillette (
    id int primary key  auto_increment,
    idParcelle int REFERENCES parcelle(id),
    quantite double DEFAULT 0 ,
    Dates date not null ,
    CHECK (quantite>=0)
);

create table salaire (
    salaire double DEFAULT 5000,
    Dates date not null
);

create table CategorieDepense (
    id int primary key  auto_increment,
    nom VARCHAR (30)
);

create table Depense (
    id int primary key  auto_increment,
    idcategorieDepense int,
    depense double,
    theDate date,
    CHECK (depense>=0),
    Foreign key (idcategorieDepense) REFERENCES idCategorieDepense(id)
);